package kvstore

import akka.actor.{Actor, ActorRef, Props, OneForOneStrategy, SupervisorStrategy}
import akka.actor.SupervisorStrategy.{Restart, Escalate}
import scala.concurrent.duration._
import kvstore.Persistence._

object PersistenceController {
  case object Tick
  def props(persistenceProps: Props): Props = Props(classOf[PersistenceController], persistenceProps)
}

/**
  * A proxy for a Persistence actor.
  * Retries unconfirmed Persist commands until a timeout of 1 second.
  */
class PersistenceController(persistenceProps: Props) extends Actor {
  import kvstore.PersistenceController._
  import context.dispatcher

  val persistence: ActorRef = context.actorOf(persistenceProps)

  // Map from message id to (requester, command, arrival timestamp)
  var acks = Map.empty[Long, (ActorRef, Persist, Long)]
  val tick = context.system.scheduler.schedule(100.millis, 100.millis, self, Tick)

  override def supervisorStrategy: SupervisorStrategy = {
    OneForOneStrategy() {
      case _: PersistenceException => Restart
      case _: Exception => Escalate
    }
  }

  override def postStop() = {
    tick.cancel()
    ()
  }

  def receive = {
    case cmd@Persist(_, _, id) =>
      val now = System.currentTimeMillis()
      persistence ! cmd
      acks += id -> Tuple3(sender, cmd, now)
    case reply@Persisted(_, id) =>
      acks.get(id).foreach {
        case (requester, _, _) =>
          requester.forward(reply)
          acks -= id
      }
    case Tick =>
      val now = System.currentTimeMillis()
      for ((id, (_, cmd, receivedAt)) <- acks) {
        if (now - receivedAt > 1000) {
          acks -= id
        } else {
          persistence ! cmd
        }
      }
  }
}
