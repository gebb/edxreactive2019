package kvstore

import akka.actor.{Actor, ActorRef, Props}
import kvstore.Arbiter._
import akka.pattern.{AskTimeoutException, ask}
import scala.concurrent.duration._
import akka.util.Timeout

object Replica {
  sealed trait Operation {
    def key: String
    def id: Long
  }
  case class Insert(key: String, value: String, id: Long) extends Operation
  case class Remove(key: String, id: Long) extends Operation
  case class Get(key: String, id: Long) extends Operation

  sealed trait OperationReply
  case class OperationAck(id: Long) extends OperationReply
  case class OperationFailed(id: Long) extends OperationReply
  case class GetResult(key: String, valueOption: Option[String], id: Long) extends OperationReply

  def props(arbiter: ActorRef, persistenceProps: Props): Props = Props(new Replica(arbiter, persistenceProps))
}

class Replica(val arbiter: ActorRef, persistenceProps: Props) extends Actor {
  import Replica._
  import Replicator._
  import Persistence._
  import PrimaryUpdateController._
  import context.dispatcher

  var kv: Map[String, String] = Map.empty
  // a map from secondary replicas to replicators
  var secondaries: Map[ActorRef, ActorRef] = Map.empty
  var snapshotSeq: Long = 0L
  val persistence: ActorRef = context.actorOf(PersistenceController.props(persistenceProps))
  var updater: ActorRef = context.actorOf(PrimaryUpdateController.props(self, persistence))

  override def preStart(): Unit = {
    arbiter ! Join
  }

  def receive = {
    case JoinedPrimary   => context.become(leader)
    case JoinedSecondary => context.become(replica)
  }

  /* Behavior for  the leader role. */
  val leader: Receive = {
    case Insert(key, value, id) =>
      kv += key -> value
      updater ! Update(sender(), key, Some(value), id)
    case Remove(key, id) =>
      kv -= key
      updater ! Update(sender(), key, None, id)
    case Get(key, id) =>
      sender ! GetResult(key, kv.get(key), id)
    case UpdateTimedOut(id, client) =>
      client ! OperationFailed(id)
    case UpdateSuccess(id, client) =>
      client ! OperationAck(id)
    case Replicas(rs) =>
      val currentReplicas = secondaries.keySet
      val newSecondaries = rs.filter(_ != self)
      val addedReplicas = newSecondaries -- currentReplicas
      val removedReplicas = currentReplicas -- newSecondaries
      for (r: ActorRef <- removedReplicas) {
        secondaries -= r
      }
      for (replica <- addedReplicas) {
        val replicator = context.actorOf(Replicator.props(replica))
        secondaries += replica -> replicator
      }
      updater ! Replicators(secondaries.values.toSet, kv)
  }

  /* Behavior for the replica role. */
  val replica: Receive = {
    case Insert | Remove =>
      throw new Exception("Tried to send update request to a replica.")
    case Get(key, id) =>
      sender ! GetResult(key, kv.get(key), id)
    case Snapshot(key, valueOption, seq) =>
      if (seq > snapshotSeq) {
        // Future request. Do nothing.
      } else if(seq < snapshotSeq) {
        // An old snapshot request that we already served.
        sender ! SnapshotAck(key, seq)
      } else {
        valueOption match {
          case Some(v) => kv += key -> v
          case None => kv -= key
        }
        snapshotSeq += 1
        val snapshotRequester = sender()
        implicit val timeout: Timeout = 1.second
        val _ = (persistence ? Persist(key, valueOption, seq)) map {
          case _: Persisted => snapshotRequester ! SnapshotAck(key, seq)
        } recover {
          case _: AskTimeoutException => ()
        }
      }
  }
}
