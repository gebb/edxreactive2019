package kvstore

import akka.actor.{Actor, ActorRef, PoisonPill, Props, Timers}
import kvstore.Persistence.{Persist, Persisted}
import kvstore.Replicator.{Replicate, Replicated}

import scala.concurrent.duration._

object PrimaryUpdateController {
  case class Replicators(replicators: Set[ActorRef], kv: Map[String, String])
  case class Update(client: ActorRef, key: String, valueOption: Option[String], id: Long)

  case class UpdateTimedOut(id: Long, client: ActorRef)
  case class UpdateSuccess(id: Long, client: ActorRef)

  def props(primary: ActorRef, persistence: ActorRef): Props =
    Props(classOf[PrimaryUpdateController], primary, persistence)
}

/**
  * Handles the IO-part of an update command for the primary replica (persistence, replication).
  * Signals a failure after a timeout of 1 second.
  */
class PrimaryUpdateController(primary: ActorRef, persistence: ActorRef) extends Actor with Timers {
  import PrimaryUpdateController._
  import context.dispatcher

  var replicators: Set[ActorRef] = Set.empty
  // A map from pending replication id to tuple
  // (persistence ready, pending replicators, client, timeout timer)
  var acks = Map.empty[Long, (Boolean, Set[ActorRef], ActorRef)]

  def receive: Receive = {
    case Replicators(newReplicators, kv) =>
      val removedReplicators = replicators -- newReplicators
      val addedReplicators = newReplicators -- replicators
      replicators = newReplicators
      discardAcks(removedReplicators)
      initReplicators(addedReplicators, kv)
    case Update(client, key, valueOption, id) =>
      timers.startSingleTimer(id, UpdateTimedOut(id, client), 1.second)
      beginUpdate(client, key, valueOption, id)
    case msg@UpdateTimedOut(id, _) =>
      if (acks.contains(id)) {
        acks -= id
        primary ! msg
      }
    case Persisted(_, id) =>
      acks.get(id) foreach {
        case (_, pending, client) =>
          acks += id -> Tuple3(true, pending, client)
          tryEndUpdate(id)
      }
    case Replicated(_, id) =>
      val replicator = sender()
      handleReplicationFinished(id, replicator)
  }

  def initReplicators(addedReplicators: Set[ActorRef], kv: Map[String, String]): Unit = {
    var id = 0
    for ((k,v) <- kv) {
      addedReplicators.foreach(_ ! Replicate(k, Some(v), id))
      id += 1
    }
  }

  private def handleReplicationFinished(id: Long, replicator: ActorRef) = {
    acks.get(id) foreach {
      case (isPersisted, pending, client) =>
        acks += id -> Tuple3(isPersisted, pending - replicator, client)
        tryEndUpdate(id)
    }
  }

  def beginUpdate(client: ActorRef,
                  key: String,
                  valueOption: Option[String],
                  id: Long): Unit = {
    acks += id -> Tuple3(false, replicators, client)
    persistence ! Persist(key, valueOption, id)
    for (replicator: ActorRef <- replicators) {
      replicator ! Replicate(key, valueOption, id)
    }
  }

  def tryEndUpdate(id: Long): Unit = {
    val (isPersisted, pending, client) = acks.get(id).get
    if (isPersisted && pending.isEmpty) {
      acks -= id
      primary ! UpdateSuccess(id, client)
      timers.cancel(id)
      ()
    }
  }

  def discardAcks(removedReplicators: Set[ActorRef]): Unit = {
    for (r: ActorRef <- removedReplicators) {
      r ! PoisonPill
      for ((id, _) <- acks) {
        handleReplicationFinished(id, r)
      }
    }
  }
}
