package protocols

import akka.actor.typed._
import akka.actor.typed.scaladsl._
import akka.actor.typed.Behavior.{interpretMessage, start, canonicalize, isUnhandled, same, validateAsInitial}

object SelectiveReceive {
    /**
      * @return A behavior that stashes incoming messages unless they are handled
      *         by the underlying `initialBehavior`
      * @param bufferSize Maximum number of messages to stash before throwing a `StashOverflowException`
      *                   Note that 0 is a valid size and means no buffering at all (ie all messages should
      *                   always be handled by the underlying behavior)
      * @param initialBehavior Behavior to decorate
      * @tparam T Type of messages
      *
      * Hint: Implement an [[ExtensibleBehavior]], use a [[StashBuffer]] and [[Behavior]] helpers such as `start`,
      * `validateAsInitial`, `interpretMessage`,`canonicalize` and `isUnhandled`.
      */
    def apply[T](bufferSize: Int, initialBehavior: Behavior[T]): Behavior[T] =
        Behaviors.setup[T] { ctx =>
            val buffer = StashBuffer[T](bufferSize)
            accumulate(bufferSize, buffer, initialBehavior)
        }

    private def accumulate[T](
        bufferSize: Int,
        buffer: StashBuffer[T],
        delegateBehavior: Behavior[T]): Behavior[T] =

        Behaviors.receive[T] { (ctx, msg) =>
            val started = validateAsInitial(start(delegateBehavior, ctx))
            val uncanonicalCurrent = interpretMessage(started, ctx, msg)
            if (isUnhandled(uncanonicalCurrent)) {
                buffer.stash(msg)
                same
            } else {
                val current = canonicalize(uncanonicalCurrent, delegateBehavior, ctx)
                buffer.unstashAll(ctx,  SelectiveReceive(bufferSize, current))
            }
        }
}
