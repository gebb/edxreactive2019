/**
 * Copyright (C) 2009-2013 Typesafe Inc. <http://www.typesafe.com>
 */
package actorbintree

import akka.actor._
import scala.collection.immutable.Queue

object BinaryTreeSet {

  trait Operation {
    def requester: ActorRef
    def id: Int
    def elem: Int
  }

  trait OperationReply {
    def id: Int
  }

  /** Request with identifier `id` to insert an element `elem` into the tree.
    * The actor at reference `requester` should be notified when this operation
    * is completed.
    */
  case class Insert(requester: ActorRef, id: Int, elem: Int) extends Operation

  /** Request with identifier `id` to check whether an element `elem` is present
    * in the tree. The actor at reference `requester` should be notified when
    * this operation is completed.
    */
  case class Contains(requester: ActorRef, id: Int, elem: Int) extends Operation

  /** Request with identifier `id` to remove the element `elem` from the tree.
    * The actor at reference `requester` should be notified when this operation
    * is completed.
    */
  case class Remove(requester: ActorRef, id: Int, elem: Int) extends Operation

  /** Request to perform garbage collection*/
  case object GC

  /** Holds the answer to the Contains request with identifier `id`.
    * `result` is true if and only if the element is present in the tree.
    */
  case class ContainsResult(id: Int, result: Boolean) extends OperationReply
  
  /** Message to signal successful completion of an insert or remove operation. */
  case class OperationFinished(id: Int) extends OperationReply

}


class BinaryTreeSet extends Actor with Stash {
  import BinaryTreeSet._
  import BinaryTreeNode._

  def createRoot: ActorRef = context.actorOf(BinaryTreeNode.props(0, initiallyRemoved = true))

  var root = createRoot

  // optional
  def receive = normal

  // optional
  /** Accepts `Operation` and `GC` messages. */
  val normal: Receive = {
    case GC =>
      val newRoot = createRoot
      root ! CopyTo(newRoot)
      context.become(garbageCollecting(newRoot))
    case op: BinaryTreeSet.Operation =>
      root ! op
  }

  // optional
  /** Handles messages while garbage collection is performed.
    * `newRoot` is the root of the new binary tree where we want to copy
    * all non-removed elements into.
    */
  def garbageCollecting(newRoot: ActorRef): Receive = {
    case GC => // Ignore.
    case op: BinaryTreeSet.Operation =>
      stash()
    case CopyFinished =>
      root ! PoisonPill
      root = newRoot
      unstashAll()
      context.become(normal)
  }
}

object BinaryTreeNode {
  trait Position

  case object Left extends Position
  case object Right extends Position

  case class CopyTo(treeNode: ActorRef)
  case object CopyFinished

  def props(elem: Int, initiallyRemoved: Boolean) = Props(classOf[BinaryTreeNode],  elem, initiallyRemoved)
}

class BinaryTreeNode(val elem: Int, initiallyRemoved: Boolean) extends Actor {
  import BinaryTreeNode._
  import BinaryTreeSet._

  var subtrees = Map[Position, ActorRef]()
  var removed = initiallyRemoved

  def delegateToChild(el: Int, op: Operation, noChildAction: () => Unit) = {
    val child = if (el > elem) subtrees.get(Right) else subtrees.get(Left)
    child match {
      case Some(c) => c ! op
      case None => noChildAction()
    }
  }

  def addChild(el: Int): ActorRef = {
    val child = context.actorOf(BinaryTreeNode.props(el, initiallyRemoved = false))
    if (el > elem) {
      subtrees = subtrees + (Right -> child)
    } else {
      subtrees = subtrees + (Left -> child)
    }
    child
  }

  // optional
  def receive = normal

  // optional
  /** Handles `Operation` messages and `CopyTo` requests. */
  val normal: Receive = {
    case op@Insert(requester, seq, el) =>
      if (el == elem) {
        removed = false
        requester ! OperationFinished(seq)
      } else {
        delegateToChild(el, op, () => addChild(el) ! op)
      }
    case op@Remove(requester, seq, el) =>
      if (el == elem) {
        removed = true
        requester ! OperationFinished(seq)
      } else {
        delegateToChild(el, op, () => requester ! OperationFinished(seq))
      }
    case op@Contains(requester, seq, el) =>
      if (el == elem) {
        requester ! ContainsResult(seq, !removed)
      } else {
        delegateToChild(el, op, () => requester ! ContainsResult(seq, false))
      }
    case op@CopyTo(target: ActorRef) =>
      if (!removed) {
        target ! Insert(self, elem, elem)
      }
      val expected = subtrees.values.toSet
      expected foreach {_ ! op}
      checkCopyFinished(expected, removed)
  }

  def checkCopyFinished(expected: Set[ActorRef], insertConfirmed: Boolean): Unit = {
    if (insertConfirmed && expected.isEmpty) {
      context.parent ! CopyFinished
      context.become(normal)
    } else {
      context.become(copying(expected, insertConfirmed))
    }
  }

  // optional
  /** `expected` is the set of ActorRefs whose replies we are waiting for,
    * `insertConfirmed` tracks whether the copy of this node to the new tree has been confirmed.
    */
  def copying(expected: Set[ActorRef], insertConfirmed: Boolean): Receive = {
    case OperationFinished(_) => checkCopyFinished(expected, true)
    case CopyFinished => checkCopyFinished(expected - sender, insertConfirmed)
  }
}
